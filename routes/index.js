var express = require('express');
var multiparty = require('multiparty');
var AWS = require('aws-sdk');
var gcloud = require('gcloud');
var fs = require('fs');
var router = express.Router();

var storage = gcloud.storage({
  projectId: 'dev-splice-91512',
  keyFilename: process.env.HOME + '/.gcloud/gcloud_keyfile.json'
});


function bytesToSize(bytes) {
  if(bytes == 0) return '0 Byte';
  var k = 1000;
  var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
  var i = Math.floor(Math.log(bytes) / Math.log(k));
  return (bytes / Math.pow(k, i)).toPrecision(3) + ' ' + sizes[i];
}

//GET home page.
router.get('/', function(req, res) {
  res.render('index', { title: 'Express' });
});

var s3 = new AWS.S3({
  region: 'eu-central-1',
  signatureVersion: 'v4'
});

//POST files stream on AWS S3
router.post('/s3', function(req, res) {
  var form = new multiparty.Form();

  form.on('error', function(err) {
    console.log('Error parsing form: ' + err.stack);
  });

  form.on('field', function(name, value) {
    console.log('Name: ' + name);
    console.log('Value: ' + value);
  });

  form.on('part', function(part) {

    var params = {Bucket: 'cppcloud', Key: part.filename, Body: part};
    var chunkSize = 5 * 1024 * 1024;
    var options;

    if (part.byteCount <= chunkSize) {
      options = {};
    } else {
      options = {partSize: chunkSize, queueSize: 1};
    }

    s3.upload(params, options, function(err, data) {
      if (err) {
        console.log(err, err.stack);
      } else {
        console.log(data);
        console.log('End upload part ' + part.filename);

        res.end('Upload success.');
      }
    })

  });

  form.parse(req);
});

//POST files on local disk
router.post('/upload', function(req, res) {
  var form = new multiparty.Form({
    uploadDir: './uploads'
  });

  form.on('file', function(name, file) {
    console.log('filename upload ' + file.originalFilename);
    console.log('Size: ' + bytesToSize(file.size));
    console.log('Path: ' + file.path);
  });

  form.parse(req);

  res.end();
});

// List AWS S3 buckets
router.get('/aws', function(req, res) {
  var s3 = new AWS.S3();

  s3.listBuckets(function(error, data) {
    if (error) {
      console.log(error);
    } else {
      console.log(data);
    }
  });

  res.end();
});

router.post('/gcloud', function(req, res) {
  var form = new multiparty.Form();

  form.on('error', function(err) {
    console.log('Error parsing form: ' + err.stack);
  });

  form.on('field', function(name, value) {
    console.log('Name: ' + name);
    console.log('Value: ' + value);
  });

  form.on('part', function(part) {
    part.pipe(storage.bucket('cpp-cloud').file(part.filename).createWriteStream())
      .on('error', function() {
        console.log('Error...' + err.stack);
      })
      .on('complete', function() {
        console.log('File ' + part.filename + ' have been uploaded!');
      });
    res.end('File is uploaded');
  });

  form.parse(req);
});

module.exports = router;
